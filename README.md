# Drupal templates for GitLab CI

GitLab CI templates for Drupal projects.
Contains gitlab-ci.yml for Drupal 8 and Drupal 7.
@see https://www.dx-experts.nl/blog/2018/drupal-8-and-gitlab-ci-setup-guide